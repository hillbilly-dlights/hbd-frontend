# hbd-frontend Changelog

## [0.0.1] The first prototype

### Fixed

*nothing*

### Added

- Create react app
- flexboxgrid
- sass
- Coming soon landing page
- Gitlab CI
- Readme
- License
- Changelog
- Git ignore
- Package.json

### Changed

*nothing*

### Removed

*nothing*
