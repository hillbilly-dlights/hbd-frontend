import React from 'react';

import { Section } from '../../components';

import { ReactComponent as DeeDeeVines } from '../../assets/svg/dee-dee-vines.svg';

export function AboutUs(){
    return (
        <Section name="about-us-section">
            <div className="col-xs-12 col-sm-7 about-us">
                <h2>A<span className="B">B</span>OUT US</h2>
                <p>
                    Have you ever watched your grandmother in the kitchen make some of the most delicious jam?! Well, I did and for years it was something I also wanted to do. I made a few jars for friends and family, and they said “hey you should sell this at the farmers market!”  Next thing ya’ know, along comes Hillbilly D’Lights, Jams, Jellies & More.
                </p>
                <p>
                    This is an exciting adventure for us and, with all the food infusion in today's market, we are finding other avenues to use the jelly and jam other than the traditional breakfast toast.  Of course we have the traditional Strawberry, Blackberry and Apple that people love in the morning, but we have more than the flavors at the grocery store!  We dove into local and exotic fruits people have never tasted, such as watermelon, elderberry, lychee, loquat & hot pepper jelly.  All of these local fruits and berries led to making infused flavors such as pineapple, mango & ginger, foxberry grape & elderberry, fireball applebutter, and many others with more exotic & tropical fruit.
                </p>
                <p>
                    We are so excited to share the love that I got to experience with my grandmother through Hillbilly D’Lights. You can jam out with us at local markets in the Nantahala, North Carolina area. We happily take custom orders for parties, weddings, and holiday gift baskets. Join us and lets start jammin’ together!
                </p>
            </div>
            <div className="col-xs-12 col-sm-5">
                <DeeDeeVines className="dee-dee-vines"/>
            </div>
        </Section>
    );
}
