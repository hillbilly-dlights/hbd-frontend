import React from 'react';

import { Section } from '../../components';
import {ReactComponent as BigJar} from '../../assets/svg/big-jar-populate-outline.svg'
import {ReactComponent as SmallJar} from '../../assets/svg/small-jar-outline.svg'
import {ReactComponent as SugarFree} from '../../assets/svg/small-jar-sugar-free-outline.svg'

function PricingProduct({size, sugar = true, price, icon, name}) {
    return (
        <article className="col-xs-6 col-sm-3 col-md-2 pricing-product">
            <header className="row">
                <div className="col-xs-12">
                    {icon()}
                </div>
            </header>
            <ul className="row">
                <li className="col-xs-12 bold">{name}</li>
                <li className="col-xs-12">{size}oz.</li>
                <li className="col-xs-12">{sugar ? 'sugar' : 'sugar free'}</li>
            </ul>
            <footer className="row center-xs">
                <div className="col-xs-12 bold">
                    $ {price}
                </div>
            </footer>
        </article>
    );
}

export function Pricing(){
    return (
        <Section name="pricing-section" alignment="middle-xs center-xs">
            <PricingProduct name="The Small" icon={SmallJar} size="8" price="6.00"/>
            <PricingProduct name="Family Size" icon={BigJar} size="10" price="8.00"/>
            <PricingProduct name="The Healthy" icon={SugarFree} size="8" sugar={false} price="7.00"/>
        </Section>
    );
}
