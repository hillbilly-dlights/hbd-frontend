import React from 'react';

import { Section } from '../../components';

import { ReactComponent as FlavorsClipBoard } from '../../assets/svg/flavors.svg';

export function Flavors(){
    return (
        <Section name="flavors-section" alignment="center-xs start-sm">
            <div className="col-xs-12 col-sm-10 col-sm-offset-2">
                <FlavorsClipBoard className="flavors"/>
            </div>
        </Section>
    );
}
