import React from 'react';

import { Section } from '../../components';

import { ReactComponent as Strawberry } from '../../assets/svg/strawberry.svg';

export function Welcome(){
    return (
        <Section name="welcome-section" alignment="center-xs">
            <div className="col-xs-12">
                <h1>Hill<span className="B">B</span>illy D'Lights</h1>
                <h2 className="row center-xs middle-xs">Jams {Strawberry()} Jellies & More</h2>
                <h3>Lake Nantahala, NC</h3>
            </div>
        </Section>
    );
}
