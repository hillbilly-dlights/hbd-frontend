import React from 'react';

import { Section } from '../../components';
import { ReactComponent as CellPhone } from '../../assets/svg/cell-phone.svg';
import { ReactComponent as Envelope } from '../../assets/svg/envelope.svg';
import { ReactComponent as Facebook } from '../../assets/svg/facebook.svg';
import { ReactComponent as Instagram } from '../../assets/svg/instagram.svg';

function Bubble({icon, text, link}) {
    return (
        <a href={link} title={text} className="col-xs-6 col-sm-2 col-md-2 col-lg-2 middle-xs bubble">
            <div className="row center-xs middle-xs">
                <div className="col-xs-12 bubble-icon">
                    {icon()}
                </div>
            </div>
        </a>
    );
}

export function ContactUs(){
    return (
        <Section name="contacts-section" alignment="center-xs">
            <div className="col-xs-12">
                <header className="row center-xs">
                    <h2>CONTACT US</h2>
                </header>
                <div className="row top-xs around-xs bubbles">
                    <Bubble icon={CellPhone} text="+1 (828) 321 9811" link="tel:+1 828 321 9811"/>
                    <Bubble icon={Envelope} text="hillbilly_dlights@yahoo.com" link="mailto:hillbilly_dlights@yahoo.com"/>
                    <Bubble icon={Facebook} text="@hillbillydlights" link="https://www.facebook.com/hillbillydlights"/>
                    <Bubble icon={Instagram} text="@hillbilly_dlights" link="https://www.instagram.com/hillbilly_dlights"/>
                </div>
            </div>
        </Section>
    );
}
