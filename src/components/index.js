import React from 'react';

export function Section({children, name, alignment = 'center-xs'}) {
    return (
        <section className={`hbd-section row middle-xs ${alignment} ${name}`}>
            {children}
        </section>
    );
}
