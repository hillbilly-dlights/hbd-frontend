import React, { Component } from 'react';
import 'flexboxgrid';
import './index.css';

import {Welcome, AboutUs, Flavors, ContactUs, Pricing} from './pods';

class App extends Component {
  render() {
    return (
      <>
        <Welcome/>
        <AboutUs/>
        <Flavors/>
        <Pricing/>
        <ContactUs/>
      </>
    );
  }
}

export default App;
